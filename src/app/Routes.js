import React, {Component} from 'react'
import {BrowserRouter, Route, Link} from 'react-router-dom';

import {Home} from './components/Home'
import {User} from './components/User'

export class Routes extends Component {
    render() {
        return (
            <div>
                <Route exact path="/home" component={Home}/>
                <Route path="/user/:id" component={User}/>
            </div>
        )
    }
}