/*import React, {Component} from 'react'
import {render} from 'react-dom'

import {Header} from './components/Header'
import {Home} from './components/Home'

class App extends Component {
    constructor(){
        super()
        this.state = {
            homeLink: 'Home'
        }
    }
    onGreet() {
        alert('Hello')
    }

    onChangeLinkName(newName){
        this.setState({
            homeLink: newName
        })
    }

    render() {
        let user = {
            name: "Anna",
            hobbies: ["Sports"]
        }
        return (
            <div className="container">
                <div className="row ">
                    <div className="col-xs-10 col-xs-offset-1">
                        <Header homeLink={this.state.homeLink}/>
                    </div>
                </div>
                <p>Text</p>
                <div className="row ">
                    <div className="col-xs-10 col-xs-offset-1">
                        <Home name={"Max"} age={27} user={user} greet={this.onGreet} changeLink={this.onChangeLinkName.bind(this)} initialLinkName={this.state.homeLink}/>
                    </div>
                </div>
            </div>
        )
    }
}

render(
    <App/>, window.document.getElementById('app'))*/
import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Switch} from 'react-router-dom';

import {Routes} from './Routes';
import {Root} from './components/Root';
import history from './history';

ReactDOM.render(
    <BrowserRouter history={ history }>
    <Root>
        <Switch>
            <Routes/>
        </Switch>
    </Root>
</BrowserRouter>, document.getElementById('app'));