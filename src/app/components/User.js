import React, {Component} from 'react'
import {browserHistory} from 'react-router'

export class User extends Component {
    onNavigateHome(){
        this.props.history.push('/home');
    }

    render() {
        return (
            <div className="container">
                <h3>The User Page</h3>
                <p>User ID: {this.props.match.params.id}</p>
                <button onClick={this.onNavigateHome.bind(this)} className="btn btn-primary">Go Home!</button>
            </div>
        )
    }
}