import React, {Component} from 'react'
import {Link} from 'react-router-dom';
/*export const Header = (props) => {
    return (
        <div className="container">
            <nav className="navbar navbar-toggleable-md navbar-light bg-faded">
                <a className="navbar-brand" href="#">{props.homeLink}</a>
            </nav>
        </div>
    )
}*/

export const Header = (props) => {
    return (
        <div className="container">
            <nav className="navbar navbar-toggleable-md navbar-light bg-faded">
                <Link to="/home" className="navbar-brand">Home</Link>
                <Link to="/user/2" className="navbar-brand">User</Link>
            </nav>
        </div>
    )
}